"""
University Name:	Kennesaw State University
College:			College of Computing and Software Engineering
Department:			Department of Computer Science
Course:				CS 4308
Course Title:		Concepts of Programming Languages
Section:			Section W01
Term:				Summer 2019
Instructor:			Dr. Jose Garrido
Student Name:		Eric Camadine
Student Email:		ecamadin@students.kennesaw.edu
Student Name:       Miles Hollis
Student Email:      mholli10@students.kennesaw.edu
Assignment:			Term Project 2nd Deliverable

SCL_Driver.py

Purpose: Defines the driver class
"""


from SCL_Parser import Parser
from SCL_Enum import Token

class Driver:
    def __init__(self):
        print("Driver init")

    def initialize_parser(self, path_to_lexemes, path_to_tokens):
        parser = Parser(path_to_lexemes, path_to_tokens)
        parser.dispatcher()     # begin!
