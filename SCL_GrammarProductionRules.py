"""
University Name:	Kennesaw State University
College:			College of Computing and Software Engineering
Department:			Department of Computer Science
Course:				CS 4308
Course Title:		Concepts of Programming Languages
Section:			Section W01
Term:				Summer 2019
Instructor:			Dr. Jose Garrido
Student Name:		Eric Camadine
Student Email:		ecamadin@students.kennesaw.edu
Student Name:       Miles Hollis
Student Email:      mholli10@students.kennesaw.edu
Assignment:			Term Project 2nd Deliverable

SCL_GrammarProductionRules.py

Purpose: Defines the production rules used by the recursive-descent parsing
"""


from SCL_Defines import Constants
from SCL_Enum import Token
from SCL_Defines import Constants


class LinkedList:
    def __init__(self, data=None, next_node=None):
        self.data = data
        self.next_node = next_node

    def get_data(self):
        return self.data

    def get_next(self):
        return self.next_node

    def set_next(self, new_next):
        self.next_node = new_next

    def insert(self, data):
        new_node = LinkedList(data)
        self.set_next(new_node)
        return new_node

class Node:
    def __init__(self, token):
        self._token = token
        self._children = []
        self._left = None
        self._center = 0
        self._right = None
        self._string = ""
        self._parent = 0

    def initialize(self, left, right):
        self._left = left
        self._right = right

    def set_string(self, string):
        self._string = string

    def set_left(self, left):
        self._left = left
        self._children.append(left)

    def set_parent(self, parent):
        self._parent = parent

    def set_right(self, right):
        self._right = right
        self._children.append(right)

    def add(self, node):
        if (self.get_left() is None or self.get_left() is 0):
            self.set_left(node)
        else:
            self.set_right(node)

    def get_left(self):
        return self._left

    def get_parent(self):
        return self._parent

    def get_right(self):
        return self._right[0]

    def get_right_by_index(self, index):
        return self._right[index]

    def get_string(self):
        return self._string

    def get_token(self):
        return self._token

    def get_children(self):
        return _children

#   the code to draw the trees was taken from
#   https://stackoverflow.com/questions/34012886/print-binary-tree-level-by-level-in-python/54074933#54074933

    def display(self):
        lines, _, _, _ = self._display_aux()
        for line in lines:
            print(line)

    def _display_aux(self):
        """Returns list of strings, width, height, and horizontal coordinate of the root."""
        # No child.
        if self._right is None and self._left is None:
            line = '%s' % self.get_string()
            width = len(line)
            height = 1
            middle = width // 2
            return [line], width, height, middle

        # Only left child.
        if self._right is None:
            lines, n, p, x = self._left._display_aux()
            s = '%s' % self.get_string()
            u = len(s)
            first_line = (x + 1) * ' ' + (n - x - 1) * '_' + s
            second_line = x * ' ' + '/' + (n - x - 1 + u) * ' '
            shifted_lines = [line + u * ' ' for line in lines]
            return [first_line, second_line] + shifted_lines, n + u, p + 2, n + u // 2

        # Only right child.
        if self._left is None:
            lines, n, p, x = self._right._display_aux()
            s = '%s' % self.get_string()
            u = len(s)
            first_line = s + x * '_' + (n - x) * ' '
            second_line = (u + x) * ' ' + '\\' + (n - x - 1) * ' '
            shifted_lines = [u * ' ' + line for line in lines]
            return [first_line, second_line] + shifted_lines, n + u, p + 2, u // 2

        # Two children.
        left, n, p, x = self._left._display_aux()
        right, m, q, y = self._right._display_aux()
        s = '%s' % self.get_string()
        u = len(s)
        first_line = (x + 1) * ' ' + (n - x - 1) * '_' + s + y * '_' + (m - y) * ' '
        second_line = x * ' ' + '/' + (n - x - 1 + u + y) * ' ' + '\\' + (m - y - 1) * ' '
        if p < q:
            left += [n * ' '] * (q - p)
        elif q < p:
            right += [m * ' '] * (p - q)
        zipped_lines = zip(left, right)
        lines = [first_line, second_line] + [a + u * ' ' + b for a, b in zipped_lines]
        return lines, n + m + u, max(p, q) + 2, n + u // 2


class Node_Value(Node):
    def __init__(self, token, value):
        Node.__init__(self, token)
        self._value = value

    def get_value(self):
        return self._value

    def set_value(self, value):
        self._value = value


class ProductionRule:
    __Token_Index = 0
    __Lexeme_Index = 0  # should be identical to token
    __Tokens_Parsed = 0

    __Stored_Token_Index = 0
    __Stored_Tokens_Parsed = 0
    __Stored_Lexemes_Index= 0

    __next_token = 0  # reserved
    __next_lexeme = ''

    __prev_token = 0
    __prev_lexeme = 0

    __LexemeArray = []
    __TokenArray = []

    __State = Constants.Production_rule_Normal

    __Root = 0

    head_node = None
    tail_node = None

    def set_state(self, new_state):
        if new_state >= Constants.Production_Rule_Start and\
            new_state <= Constants.Production_Rule_End:
            self.__State = new_state
        else:
            print("Invalid argument to set_state.")

    def get_state(self):
        return self.__State

    def initialize(self, index):
        self.__Tokens_Parsed = 0
        self.__Token_Index = index
        self.__Lexeme_Index = index

    def __init__(self, lexeme_array, token_array):
        self.__LexemeArray = lexeme_array
        self.__TokenArray = token_array

    def advance_lexeme(self):
        self.__Lexeme_Index += 1

    def retract_lexeme(self):
        self.__Lexeme_Index -= 1

    def advance_token(self):
        if (self.__Token_Index + 1) < len(self.__TokenArray):
            self.__Token_Index += 1
            self.__Tokens_Parsed += 1
            self.advance_lexeme()
        else:
            print("out of bounds in advance_token")

    def retract_token(self):
        if (self.__Token_Index - 1) >= 0:
            self.__Token_Index -= 1
            self.__Tokens_Parsed -= 1
            self.retract_lexeme()
        else:
            print("out of bounds in get_previous_token")

    def get_next_token(self):
        self.__prev_token = self.__next_token

        self.__next_token = self.__TokenArray[self.__Token_Index]

        self.advance_token()

        self.set_state(Constants.Production_Rule_Request_Token)

        print("next lexeme")
        print(self.get_next_lexeme())

        return self.__next_token

    def get_current_token(self):
        return self.__TokenArray[self.__Token_Index]

    def get_token_at_index(self, index):
        if (self.__Token_Index + index) < len(self.__TokenArray) and \
            (self.__Token_Index + index) >= 0:
            return self.__TokenArray[self.__Token_Index]
        else:
            print("out of bounds in get_token_at_index")

    def get_previous_token(self):
        return self.__prev_token

    def store_token_index(self):
        self.__Stored_Lexemes_Index = self.__Lexeme_Index
        self.__Stored_Token_Index = self.__Token_Index
        self.__Stored_Tokens_Parsed = self.__Tokens_Parsed

    def restore_token_index(self):
        self.__Lexeme_Index = self.__Stored_Lexemes_Index
        self.__Token_Index = self.__Stored_Token_Index
        self.__Tokens_Parsed = self.__Stored_Tokens_Parsed

    def get_next_lexeme(self):
        self.__next_lexeme = self.__LexemeArray[self.__Lexeme_Index]

        next_lexeme = self.__next_lexeme.strip("Lexeme: ")
        next_lexeme = next_lexeme.strip("\n")

        return next_lexeme

    def get_number_of_parsed_tokens(self):
        return self.__Tokens_Parsed

    def finish(self):

        current = self.head_node.get_next()

        while True:

            if current is None:
                break

            current.get_data().display()

            current = current.get_next()

            print ("")
            print ("")


    def const_var_struct(self):
        print ("<const_var_struct>")

        #self.const_dec()

        self.var_dec()

        #self.struct_dec()

    def pother_oper_def(self):
        print("<pother_oper_def>")

        node = Node(self.get_current_token())
        node.set_string("<pother_oper_def>")

        current_token = self.get_current_token()

        if current_token == Token.Identifier or\
            current_token == Token.MAIN:
            # ignore description, it's just comments
            # do not consider ret type, yet this is just a simple implementation

            self.advance_token()

            current_token = self.get_current_token()

            if current_token == Token.PARAMETERS:
                print ("parameters")
            else:
                print ("no parameters")

            if current_token == Token.IS:
                self.advance_token()

                self.const_var_struct()

                #self.precond()

                current_token = self.get_current_token()

                if current_token == Token.BEGIN:
                    new_node = Node(self.get_current_token())
                    new_node.set_string("<begin>")
                    node.add(new_node)

                    print ("<begin>")

                    self.advance_token()

                    current_token = self.get_current_token()

                    while current_token is not Token.ENDFUN:

                        new_node = self.action_def(current_token)

                        current_token = self.get_current_token()

                        self.tail_node = self.tail_node.insert(new_node)

                    new_node = self.endfun()

                    node.add(new_node)

        return node

    def funct_body(self):
        print("<funct_body>")

        node = Node(self.get_current_token())
        node.set_string("<funct_body>")

        current_token = self.get_current_token()

        if current_token == Token.FUNCTION:
            self.advance_token()

            # self.phead_func()

            new_node = self.pother_oper_def()
            node.add(new_node)

        return node

    def funct_list(self):
        print ("<funct_list>")

        node = Node(self.get_current_token())
        node.set_string("<funct_list>")

        new_node = self.funct_body()

        node.add(new_node)

        return node

    def main_head(self):
        print ("<main_head>")

        node = Node(self.get_current_token())
        node.set_string("<main_head>")

        new_node = self.funct_list()

        node.add(new_node)

        return node


    def implement(self):
        print ("<implement>")

        node = Node(self.get_current_token())
        node.set_string("<implement>")
        self.tail_node = self.tail_node.insert(node)

        new_node = self.main_head()
        node.add(new_node)

    def globals(self):
        print ("<globals>")

        current_token = self.get_current_token()

        if current_token == Token.GLOBAL:
            node = Node(self.get_current_token())
            node.set_string("<globals>")
            self.tail_node = self.tail_node.insert(node)

            self.advance_token()

            self.const_dec()
            self.var_dec()
            #self.struct_dec

    def _import(self):
        print ("import")

        node = Node(self.get_current_token())

        self.tail_node = self.tail_node.insert(node)

        self.advance_token()

        lexeme_node = Node_Value(self.get_current_token(), self.get_next_lexeme())
        lexeme_node.set_string(self.get_next_lexeme())

        node.set_string("<import>")
        node.add(lexeme_node)

    def stage(self, token):
        node = None

        if self.head_node is None:
            self.head_node = LinkedList()
            self.tail_node = self.head_node

        self.set_state(Constants.Production_Rule_Start)
        if token == Token.IMPORT:
            self._import()

        if token == Token.IMPLEMENTATIONS:
            self.advance_token()

            self.implement()

        if token == Token.GLOBAL:
            self.globals()

        if self.get_state() != Constants.Production_Rule_Error:
            self.set_state(Constants.Production_Rule_End)
            print("Successfully parsed expressions.")
        else:
            print("Had error while parsing. Attempting to recover.")

        return node

    def pactions(self, token):
        self.action_def(token)

    def action_def(self, token):
        print("<action_def>")

        node = None

        if token == Token.SET:
            node = self.set()
        elif token == Token.READ:
            node = self.pvar_value_list()
        elif token == Token.INPUT:
            print("input")
        elif token == Token.DISPLAY:
            node = self.display()
        elif token == Token.DISPLAYN:
            node = self.displayn()
        elif token == Token.MCLOSE:
            print("mclose")
        elif token == Token.MFILE:
            print("mfile")
        elif token == Token.INCREMENT:
            node = self.name_ref()
        elif token == Token.DECREMENT:
            node = self.name_ref()
        elif token == Token.RETURN:
            node = self._return()
        elif token == Token.CALL:
            node = self.call()
        elif token == Token.IF:
            print("if")
        elif token == Token.FOR:
            print("for")
        elif token == Token.WHILE:
            print("while")
        elif token == Token.CASE:
            print("case")
        elif token == Token.MBREAK:
            print("mbreak")
        elif token == Token.MEXIT:
            print("mexit")
        elif token == Token.ENDFUN:
            node = self.endfun()
        elif token == Token.EXIT:
            node = self.exit()
        elif token == Token.POSTCONDITION:
            print("postcondition")

        return node

    def exit(self):
        print ("<exit>")

        node = None

        node = Node(self.get_current_token())

        node.set_string("<exit>")

        self.advance_token()

        return node

    def endfun(self):
        print ("<endfun>")

        node = Node(self.get_current_token())
        node.set_string("<endfun>")

        self.advance_token()

        current_token = self.get_current_token()

        if current_token == Token.Identifier or \
            current_token == Token.MAIN:
            print ("finished parsing.")

            new_node = Node(current_token)

            node.add(new_node)

            if current_token == Token.MAIN:
                new_node.set_string("MAIN")
            else:
                new_node.set_string(self.get_next_lexeme())

        else:
            print("Missing token for identifier or main in endfun")

            self.set_state(Constants.Production_Rule_Error)

        return node

#   def implement(self):


    def var_dec(self):
        print("<var_dec>")

        self.advance_token()

        current_token = self.get_current_token()

        variable_node = Node(current_token)
        variable_node.set_string("<declare>")

        node = self.data_declarations(variable_node)

        self.tail_node.insert(node)

    def call(self):
        print("<call>")

        current_token = self.get_current_token()

        call_node = Node(current_token)
        call_node.set_string("<call>")

        self.advance_token()

        node = self.name_ref(call_node)
        # pusing_ref

        call_node.add(node)
        node.set_parent(call_node)

        self.tail_node.insert(call_node)

        return node

    def _return(self):
        print("<return>")

        current_token = self.get_current_token()

        return_node = Node(current_token)
        return_node.set_string("<return>")

        self.advance_token()

        node = self.expr(return_node)

        return_node.set_right(node)
        node.set_string("<expr>")
        node.set_parent(return_node)

        self.tail_node.insert(return_node)

        return node

    def const_dec(self):
        print("<const_dec>")

        node = None

        current_token = self.get_current_token()

        if current_token == Token.CONSTANTS:
            node = self.data_declarations()
            node.set_string("<const_dec>")
            self.tail_node.insert(node)
        else:
            print("Missing token for constants declaration in const_dec")

            self.set_state(Constants.Production_Rule_Error)

        return node

    def iconst_ident(self):
        print("<iconst_ident>")

        node = None

        current_token = self.get_current_token()

        if current_token == Token.IntegerConstant or\
            current_token == Token.Identifier:
            self.advance_token()

            if current_token == Token.IntegerConstant:
                node = Node_Value(current_token, int(self.get_next_lexeme(), 10))
                node.set_string("<constant>")
            else:
                node = Node(current_token)
                node.set_string("<identifier>")

            self.tail_node.insert(node)
        else:
            print("unknown token in comp_declare")

            self.set_state(Constants.Production_Rule_Error)

        return node

    def data_declaration(self, parent):
        print("<data_declaration>")

        node = None

        current_token = self.get_current_token()

        if current_token == Token.Identifier:
            self.advance_token()

            node = Node(current_token)
            node.set_string("<identifier>")

            print("Token_Identifier")

            if current_token == Token.ARRAY or\
                current_token == Token.LeftBracket or\
                current_token == Token.VALUE or\
                current_token == Token.Assignment:

                print("empty case!")
            else:
                current_token = self.get_current_token()

                self.advance_token()

                if current_token == Token.OF:

                    current_token = self.get_current_token()

                    self.advance_token()

                    if current_token == Token.TYPE:
                        current_token = self.get_current_token()

                        self.advance_token()

                        if current_token == Token.TUNSIGNED or \
                            current_token == Token.CHAR or \
                            current_token == Token.INTEGER or \
                            current_token == Token.MVOID or \
                            current_token == Token.DOUBLE or \
                            current_token == Token.LONG or \
                            current_token == Token.SHORT or \
                            current_token == Token.FLOAT or \
                            current_token == Token.REAL or \
                            current_token == Token.TSTRING or \
                            current_token == Token.TBOOL or \
                            current_token == Token.TBYTE:

                            new_node = Node(current_token)

                            parent.initialize(node, new_node)

                            node = parent
                        else:
                            print("Unsupported data type in data_declaration")

                            self.set_state(Constants.Production_Rule_Error)

                    else:
                        print("Missing TYPE in define in data_declaration")

                        self.set_state(Constants.Production_Rule_Error)


                else:
                    print("Missing OF in define in data_declaration")

                    self.set_state(Constants.Production_Rule_Error)

        return node

    def data_file(self, parent):
        print("<data_file>")

        node = None

        node = self.data_declaration(parent)

        return node

    def comp_declare(self, parent):
        print("<comp_declare>")

        node = None

        current_token = self.get_current_token()

        if current_token == Token.DEFINE:
            self.advance_token()

            node = self.data_file(parent)

        else:
            print("missing define token in comp_declare")

            self.set_state(Constants.Production_Rule_Error)

        return node

    def data_declarations(self, parent):
        print("<data_declarations>")

        node = self.comp_declare(parent)

        return node

    def define(self):
        print("<define>")

        current_token = self.get_current_token()

        define_node = Node(current_token)
        define_node.set_string("<define>")

        node = self.data_declarations(define_node)

        define_node.add(node)
        node.set_parent(define_node)

        return define_node

    def plist_const(self):
        print("<plist_const>")

        node = None

        current_token = self.get_current_token()

        if current_token == Token.LeftBracket:
            self.advance_token()

            node = self.iconst_ident()

            current_token = self.get_current_token()

            if current_token != Token.RightBracket:
                print("missing right bracket in plist_const")

                self.set_state(Constants.Production_Rule_Error)
        else:
            print("missing left bracket in plist_const")

            self.set_state(Constants.Production_Rule_Error)

        return node

    def opt_ref(self):
        print("<opt_ref>")
        node = self.array_val()

        return node

    def name_ref(self):
        print("<name_ref>")

        current_token = self.get_current_token()

        if current_token != Token.Identifier:
            print("missing identifier in name_ref")

            self.set_state(Constants.Production_Rule_Error)
        else:
            node = self.opt_ref()

        return node

    def display(self):
        print("<display>")

        node = self.pvar_value_list()

        return node

    def displayn(self):
        print("<displayn>")

        node = self.pvar_value_list()

        return node

    def pvar_value_list(self):
        print("<pvar_value_list>")

        # first expr

        arg_list_node = Node(self.get_current_token())
        arg_list_node.set_string("<pvar_value_list>")

        self.advance_token()

        head = self.expr(arg_list_node)
        head.set_parent(arg_list_node)
        arg_list_node.add(head)

        while True:
            current_token = self.get_current_token()

            if current_token != Token.Comma:
                break
            else:
                self.advance_token()

                node = self.expr(head)
                arg_list_node.add(node)
                node.set_parent(arg_list_node)

        return arg_list_node

    def arg_list(self):
        print("<arg_list>")

        arg_list_node = Node(self.get_current_token())
        arg_list_node.set_string("<pvar_value_list>")

        self.advance_token()

        head = self.expr(arg_list_node)
        head.set_parent(arg_list_node)
        arg_list_node.add(head)

        while True:
            current_token = self.get_current_token()

            if current_token != Token.Comma:
                break
            else:
                self.advance_token()

                node = self.expr()
                arg_list_node.add(node)
                node.set_parent(arg_list_node)

        return arg_list_node

    def array_val(self):
        print("<array_val>")
        self.simp_arr_val()

    def simp_arr_val(self):
        print("<simp_arr_val>")

        node = None

        current_token = self.get_current_token()

        if current_token == Token.LeftBracket:
            node = self.arg_list()

            current_token = self.get_current_token()

            if current_token != Token.RightBracket:
                print("missing right bracket in array_val")

                self.set_state(Constants.Production_Rule_Error)

        else:
            print("missing left bracket in array_val")

            self.set_state(Constants.Production_Rule_Error)

        return node

    def parguments(self):
        print("<parguments>")

        node = None

        current_token = self.get_current_token()

        if current_token == Token.LeftParentheses:
            node = self.arg_list()

            current_token = self.get_current_token()

            if current_token != Token.RightParentheses:
                print("missing right parentheses in parguments")

        else:
            print("missing left parentheses in parguments")

        return node

    def type_name(self):
        print("<type_name>")

        node = None

        current_token = self.get_current_token()

        self.advance_token()

        if current_token == Token.MVOID or \
            current_token == Token.INTEGER or \
            current_token == Token.SHORT or \
            current_token == Token.REAL or \
            current_token == Token.FLOAT or \
            current_token == Token.DOUBLE or \
            current_token == Token.TBOOL or \
            current_token == Token.CHAR or \
            current_token == Token.TSTRING or \
            current_token == Token.TBYTE:

            if current_token == Token.TSTRING:
                self.advance_token()

                current_token = self.get_current_token()

                if current_token != Token.OF:
                    print("missing OF in type_name")

                    self.set_state(Constants.Production_Rule_Error)
                else:

                    if current_token != Token.LENGTH:
                        print("missing LENGTH in type_name")

                        self.set_state(Constants.Production_Rule_Error)
                    else:
                        node = Node(current_token)

            else:
                 node = Node(current_token)
        else:
            print("missing LENGTH in type_name")

            self.set_state(Constants.Production_Rule_Error)

        return node

    def ret_type(self, parent):
        print("<ret_type>")

        node = None

        current_token = self.get_current_token()

        self.advance_token()

        if current_token == Token.TYPE:
            node = self.type_name()
        elif current_token == Token.STRUCT or\
            current_token == Token.STRUCTYPE:

            current_token = self.get_current_token()

            self.advance_token()

            if current_token == Token.IDENTIFIER:
                node = Node(current_token)
                node.set_parent(parent)
                node.set_string("<ret_type>")

                self.tail_node = self.tail_node.insert(node)
            else:
                print("missing identifier in ret_type")

                self.set_state(Constants.Production_Rule_Error)

        return node

    def element(self, parent):
        print("<element>")

        node = None

        did_match = 0

        current_token = self.get_current_token()

        if current_token == Token.Identifier or\
            current_token == Token.String or\
            current_token == Token.Letter or \
            current_token == Token.IntegerConstant or\
            current_token == Token.HexadecimalConstant or\
            current_token == Token.FloatingPointConstant or\
            current_token == Token.MTRUE or\
            current_token == Token.MFALSE:

            if current_token == Token.Identifier:

                current_token = self.get_current_token()

                self.advance_token()

                if current_token == Token.LeftParentheses: # parguments
                    node = self.parguments()  # return node
                elif current_token == Token.LeftBracket: #array_val
                    node = self.array_val()  # return node
                else:
                    node = Node(Token.Identifier)
                    node.set_string("<element>")
            else:
                if current_token == Token.String:
                    node = Node_Value(current_token, self.get_next_lexeme)
                    did_match = 1

                elif current_token == Token.Letter:
                    node = Node_Value(current_token, self.get_next_lexeme)
                    did_match = 1

                elif current_token == Token.IntegerConstant:
                    node = Node_Value(current_token, int(self.get_next_lexeme(), 10))
                    did_match = 1

                elif current_token == Token.HexadecimalConstant:
                    node = Node_Value(current_token, hex(self.get_next_lexeme()))
                    did_match = 1

                elif current_token == Token.FloatingPointConstant:
                    node = Node_Value(current_token, float(self.get_next_lexeme()))
                    did_match = 1

                elif current_token == Token.MTRUE or current_token == Token.MFALSE:
                    node = Node_Value(current_token, bin(self.get_next_lexeme()))
                    did_match = 1

                if did_match == 1:
                    new_node = Node(self.get_current_token())
                    new_node.set_string(self.get_next_lexeme())
                    new_node.set_parent(parent)
                    node.add(new_node)

                    self.advance_token()
                    node.set_string("<element>")
                    node.set_parent(parent)



        elif current_token == Token.LeftParentheses:

            node = self.expr(parent)

            if current_token != Token.RightParentheses:

                print("missing closing right parentheses in element")

                self.set_state(Constants.Production_Rule_Error)
        else:
            print("unknown token in element")

            self.set_state(Constants.Production_Rule_Error)

        return node

    def punary(self, parent):
        print("<punary>")

        node = self.element(parent)

        did_match = 0
        new_node = 0

        current_token = self.get_current_token()

        if current_token == Token.Minus:
            new_node = Node(Token.Minus)
            new_node.initialize(node, self.element(node))
            did_match = 1

        elif current_token == Token.NEGATE:
            new_node = Node(Token.NEGATE)
            did_match = 1

        if did_match == 1:
            self.advance_token()
            new_node.set_string("<punary>")
            new_node.set_parent(parent)
            return new_node
        else:
            new_node = Node(self.get_current_token())
            new_node.set_string("<punary>")
            new_node.set_parent(parent)
            new_node.add(node)
            return new_node

        return node

    def term(self, parent):
        print("<term>")

        node = self.punary(parent)
        did_match = 0

        current_token = self.get_current_token()

        if current_token == Token.Asterisk:
            did_match = 1

        elif current_token == Token.Division:
            did_match = 1

        elif current_token == Token.Modulus:
            did_match = 1

        elif current_token == Token.LSHIFT:
            did_match = 1

        elif current_token == Token.RSHIFT:
            did_match = 1

        if did_match == 1:
            new_node = Node(current_token)
            self.advance_token()
            new_node.initialize(node, self.punary(new_node))
            new_node.set_string("<term>")
            node.set_parent(new_node)

            current_token = self.get_current_token()

            self.advance_token()

            if current_token == Token.Asterisk or \
                current_token == Token.Division or \
                current_token == Token.Modulus or \
                current_token == Token.LSHIFT or \
                current_token == Token.RSHIFT:

                node = Node(current_token)
                recursive = self.term(parent)
                node.initialize(new_node, recursive)
                node.set_string("<term>")
                new_node.set_parent(node)
            else:

                self.retract_token()

                return new_node

        return node

    def expr(self, parent):
        print("<expr>")

        node = self.term(parent)
        node.set_string("<expr>")

        did_match = 0
        new_node = 0

        current_token = self.get_current_token()

        if current_token == Token.Plus:
            new_node = Node(Token.Plus)
            did_match = 1

        elif current_token == Token.Minus:
            new_node = Node(Token.Minus)
            did_match = 1

        elif current_token == Token.BAND:
            new_node = Node(Token.BAND)
            did_match = 1

        elif current_token == Token.BOR:
            new_node = Node(Token.BOR)
            did_match = 1

        elif current_token == Token.BXOR:
            new_node = Node(Token.BXOR)
            did_match = 1

        elif current_token == Token.Exponent:
            new_node = Node(Token.Exponent)
            did_match = 1

        if did_match == 1:
            self.advance_token()
            new_node.initialize(node, self.term(new_node))
            new_node.set_string("<expr>")
            node.set_parent(new_node)

            node = Node(current_token)

            self.tail_node = self.tail_node.insert(node)

            return new_node

        return node

    def set(self):
        print("<set>")

        set_node = None

        current_token = self.get_current_token()

        self.advance_token()

        if current_token == Token.SET:
            set_node = Node(current_token)
            set_node.set_string("<set>")

            current_token = self.get_current_token()

            # if the first token after set is not an identifier, throw an error

            if current_token != Token.Identifier:
                print("missing identifier in set")

                self.set_state(Constants.Production_Rule_Error)
            else:

                identifier_node = Node_Value(Token.Identifier, self.get_next_lexeme())

                self.advance_token()

                identifier_node.set_string("<id>")

                set_node.set_left(identifier_node)
                identifier_node.set_parent(set_node)

                current_token = self.get_current_token()

                if current_token != Token.Assignment:
                    print("missing assignment token in set")

                    self.set_state(Constants.Production_Rule_Error)
                else:
                    # look at the first expr

                    self.advance_token()

                    node = self.expr(set_node)

                    set_node.set_right(node)
                    node.set_parent(set_node)
        else:
            print("invalid token in set")

            self.set_state(Constants.Production_Rule_Error)

        return set_node

    """
    precond ::=
    | PRECONDITION pcondition

    pcondition ::= pcond1 OR pcond1
        | pcond1 AND pcond1
        | pcond1
    
    pcond1 ::= NOT pcond2
        | pcond2
    
    pcond2 ::= LP pcondition RP
        | expr RELOP expr
        | expr EQOP expr
        | expr eq_v expr
        | expr opt_not true_false
        | element
    
    true_false ::= MTRUE
        | MFALSE
    eq_v ::= EQUALS
        | GREATER THAN
        | LESS THAN
        | GREATER OR EQUAL
        | LESS OR EQUAL
    
    opt_not ::=
        | NOT
"""
    def precond(self):
        self.pcondition()

    def pcondition(self):
        print("<pcondition>")

        current_token = self.get_next_token()

        self.advance_token()

        node_pcondition = Node(current_token)
        node_pcondition.set_string("<opt_not>")

    def pcond1(self):
        print("<pcond1>")

        current_token = self.get_next_token()

        if current_token != Token.Not:

            node_not = Node(current_token)
            node_not.set_string("<opt_not>")

            self.tail_node.insert(node_not)

            node = self.pcond2(node_not)

            node_not.add(node)
        else:
            node = Node(current_token)

            node = self.pcond2()

            self.tail_node.insert(node)

        return node

    def pcond2(self):
        print("<pcond2>")

        node = None

        current_token = self.get_next_token()

        if current_token == Token.LeftParentheses:
            self.advance_token()

            self.pcondition()

            if current_token != Token.RightParentheses:
                print("missing closing right parentheses in pcond2")

                self.set_state(Constants.Production_Rule_Error)
        else:
            current_token = self.get_current_token()

            self.advance_token() # should be on expr, if we did element then no relationals

            node_expr = self.expr()

            self.advance_token()

            if current_token == Token.Relational or \
                current_token == Token.Relational_Equals:
                node_expr2 = self.expr()

            self.tail_node.add(node_expr)
            self.tail_node.add(node_expr2)





#           else:
#                node = self.element()

    def true_false(self):
        print("<true_false>")

        node = None

        current_token = self.get_next_token()

        if current_token == Token.MTRUE or\
            current_token == Token.MFALSE:

            self.advance_token()

            node = Node(current_token)
            node.set_string("<true_false>")

            self.tail_node.insert(node)
        else:
            print("missing true or false token in true_false")

            self.set_state(Constants.Production_Rule_Error)

        return node

    def eq_v(self):
        print("<eq_v>")

        node = None

        current_token = self.get_next_token()

        if current_token == Token.Equality or \
            current_token == Token.Relational or\
            current_token == Token.Relational_Equals:

            self.advance_token()

            node = Node(current_token)
            node.set_string("<eq_v>")

            self.tail_node.insert(node)

        else:
            print("missing equality token in opt_not")

            self.set_state(Constants.Production_Rule_Error)

        return node

    def opt_not(self):
        print("<opt_not>")

        node = None

        current_token = self.get_next_token()

        if current_token != Token.Not:
            print("missing not token in opt_not")

            self.set_state(Constants.Production_Rule_Error)
        else:
            self.advance_token()

            node = Node(current_token)
            node.set_string("<opt_not>")

            self.tail_node.insert(node)

        return node
