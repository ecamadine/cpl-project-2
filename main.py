"""
University Name:	Kennesaw State University
College:			College of Computing and Software Engineering
Department:			Department of Computer Science
Course:				CS 4308
Course Title:		Concepts of Programming Languages
Section:			Section W01
Term:				Summer 2019
Instructor:			Dr. Jose Garrido
Student Name:		Eric Camadine
Student Email:		ecamadin@students.kennesaw.edu
Student Name:       Miles Hollis
Student Email:      mholli10@students.kennesaw.edu
Assignment:			Term Project 2nd Deliverable

main.py

Purpose: Defines the entry point for the application
"""

import sys
import os

from SCL_Driver import Driver


def main():
#   filenane_lexeme = sys.argv[1]
#   filename_token = sys.argv[2]
    testDriver = Driver()

    lexeme_filename = raw_input("Enter lexeme filename.")
    token_filename = raw_input("Enter token filename.")

    testDriver.initialize_parser(lexeme_filename, token_filename)



main()
