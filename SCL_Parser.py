"""
University Name:	Kennesaw State University
College:			College of Computing and Software Engineering
Department:			Department of Computer Science
Course:				CS 4308
Course Title:		Concepts of Programming Languages
Section:			Section W01
Term:				Summer 2019
Instructor:			Dr. Jose Garrido
Student Name:		Eric Camadine
Student Email:		ecamadin@students.kennesaw.edu
Student Name:       Miles Hollis
Student Email:      mholli10@students.kennesaw.edu
Assignment:			Term Project 2nd Deliverable

main.py

Purpose: Defines the parser class that completes the assignment guidelines
"""


from SCL_Enum import Token
from SCL_GrammarProductionRules import ProductionRule


class FileNotFound(Exception):
    pass


class Parser:

    __line_number_token = 0
    __line_number_lexeme = 0

    __Token_Current_Position = 0
    __Lexeme_Current_Position = 0

    __TokenArray = []
    __LexemeArray = []

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.__LexemeFile.close()
        self.__TokenFile.close()

    def __init__(self, path_to_lexemes, path_to_tokens):
        print("Parser init")

        self.__lexeme_path = path_to_lexemes
        self.__token_path = path_to_tokens

        try:
            self.__LexemeFile = open(self.__lexeme_path, "r")
            self.__TokenFile = open(self.__token_path, "r")

            while True:
                returned_lexeme = self.get_lexeme()
                returned_token = self.get_token()

                if returned_token == -1:
                    break

                elif returned_token != Token.Default:
                    self.__TokenArray += [returned_token]

                if returned_lexeme != "":
                    if returned_lexeme != '\n':
                        self.__LexemeArray += [returned_lexeme]

            self.__ProductionRule = ProductionRule(self.__LexemeArray, self.__TokenArray)

        except FileNotFound:
            print("Terminating application.")

    def dispatcher(self):
        x = 0

        while x < len(self.__TokenArray):
            index_adjust = self.dispatch(self.__TokenArray[x], x)
            x += index_adjust

        self.__ProductionRule.finish()

    def dispatch(self, token, index):
        self.__ProductionRule.initialize(index)

        node = self.__ProductionRule.stage(token)

        index_adjust = self.__ProductionRule.get_number_of_parsed_tokens()

        if index_adjust == 0: # this should not happen once all tokens are supported...
            index_adjust = 1

        # if we had error, adjust index_adjust to get us to the next set of tokens

        return index_adjust

    def update(self):
        self.__Token_Current_Position = self.__TokenFile.tell()
        self.__Lexeme_Current_Position = self.__TokenFile.tell()

#   Get the next token from the "lexer"
    def get_token(self):

        return_value = -1

        line_token = self.__TokenFile.readline()

        self.__line_number_token += 1

        number_string = ""

        if line_token:
            for s in line_token:
                if s.isdigit():
                    number_string += s
                    return_value = int(number_string, 10) # if done inside the if line: then issue

        if line_token and return_value == -1:
            return_value = Token.Default

        return return_value

    def get_lexeme(self):
        lexeme = ""

        line_lexeme = self.__LexemeFile.readline()

        self.__line_number_lexeme += 1

        if line_lexeme:
            for s in line_lexeme:
 #              print s
                lexeme += s

        return lexeme
